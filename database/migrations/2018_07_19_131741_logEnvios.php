<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogEnvios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landings_enviados_x_servicios', function (Blueprint $table) {
            $table->text('datos_envio')->nullable()->default(null);
            $table->text('respuesta')->nullable()->default(null);
        });
        Schema::table('formularios_enviados_x_servicios', function (Blueprint $table) {
            $table->text('datos_envio')->nullable()->default(null);
            $table->text('respuesta')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
