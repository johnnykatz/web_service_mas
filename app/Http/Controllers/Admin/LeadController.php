<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use App\Repositories\Admin\LeadRepository;
use Maatwebsite\Excel\Facades\Excel;

class LeadController extends InfyOmBaseController
{
    private $leadRepository;

    public function __construct(LeadRepository $leadRepo)
    {
        $this->leadRepository = $leadRepo;
    }

    public function index(Request $request)
    {
        $leads = $this->leadRepository->getLeadsFilter($request);

        return view('admin.leads.index')
            ->with([
                'origins' => $leads['select_origin'],
                'forms' => $leads['select_form'],
                'leads' => $leads['table'],
                'filters' => $request->all()
            ]);
    }

    public function export_leads(Request $request)
    {
        $leads = $this->leadRepository->getLeadsFilter($request);
        $leads = $leads['table'];

        Excel::create('Leads ' . date('d-m-Y H:i:s'), function ($excel) use ($leads) {
            $excel->sheet('Leads', function ($sheet) use ($leads) {
                $sheet->fromArray($leads);
            });
        })->export("xls");
    }
}
