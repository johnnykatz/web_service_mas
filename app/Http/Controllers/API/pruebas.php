<?php

namespace App\Http\Controllers\API;


use App\Models\Admin\AsociacionCamposServicios;
use App\Models\Admin\Crm;
use App\Models\Admin\Formulario;
use App\Models\Admin\Landing;
use App\Models\Admin\LandingsCamposServicio;
use App\Models\Admin\ServicioCrm;
use App\Models\Admin\ServicioCrmXFormulario;
use App\Models\Admin\Token;

use App\Providers\FacebookProvider;
use App\Providers\FuncionesProvider;
use App\Services\CrmService;
use App\Services\LandingsService;
use Doctrine\DBAL\Schema\Schema;
use FacebookAds\Api;
use FacebookAds\Object\Page;
use FacebookAds\Object\LeadgenForm;
use FacebookAds\Object\Fields\AdReportRunFields;

use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Database\Schema\Blueprint;
use Maatwebsite\Excel\Facades\Excel;

class pruebas extends Controller
{
    function prueba()
    {
        /*$table = $this->get_leads();

        usort($table, function ($a, $b) {
            return strtotime($b['created_at']) - strtotime($a['created_at']);
        });

        dd($table);*/

        /*$this->asociarOrigen();
        $this->crearCampo();*/

//        $this->export_excel_leads_forms();
        $this->export_excel_leads_landings();
    }

    private function get_leads()
    {
        $landings = Formulario::where('con_estructura', true)->get();
        $table = array();

        foreach ($landings as $l) {
            $structure = $this->structure($l->db_name);
            $leads = $this->leads($l->db_name, $structure);
            $i = 0;
            foreach ($leads as $lead) {
                $data = array('fullname' => null, 'email' => null, 'phone' => null, 'city' => null, 'created_at' => null);
                foreach ($lead as $key => $value) {
                    if ($key == 'firstname' || $key == 'lastname' || $key == 'fullname' || $key == 'name' || $key == 'nombre' || $key == 'nombres' || $key == 'lastname') {
                        $data['fullname'] .= $leads[$i][$key] . ' ';
                    }
                    if ($key == 'email') {
                        $data['email'] = $leads[$i][$key];
                    }
                    if ($key == 'phonenumber' || $key == 'phone' || $key == 'celular' || $key == 'telefono') {
                        $data['phone'] = $leads[$i][$key];
                    }
                    if ($key == 'city' || $key == 'ciudad') {
                        $data['city'] = $leads[$i][$key];
                    }
                    $data['created_at'] = $leads[$i]['created_at'];
                }
                $table[] = $data;
                $i++;
            }
        }

        $landings = Landing::where('con_estructura', true)->get();

        foreach ($landings as $l) {
            $structure = $this->structure($l->db_name);
            $leads = $this->leads($l->db_name, $structure);
            $i = 0;
            foreach ($leads as $lead) {
                $data = array('fullname' => null, 'email' => null, 'phone' => null, 'city' => null);
                foreach ($lead as $key => $value) {
                    if ($key == 'full_name' || $key == 'firstname' || $key == 'lastname' || $key == 'fullname' || $key == 'name' || $key == 'nombre' || $key == 'nombres' || $key == 'lastname') {
                        $data['fullname'] .= $leads[$i][$key] . ' ';
                    }
                    if ($key == 'email') {
                        $data['email'] = $leads[$i][$key];
                    }
                    if ($key == 'phonenumber' || $key == 'phone' || $key == 'celular' || $key == 'telefono') {
                        $data['phone'] = $leads[$i][$key];
                    }
                    if ($key == 'city' || $key == 'ciudad') {
                        $data['city'] = $leads[$i][$key];
                    }
                    $data['created_at'] = $leads[$i]['created_at'];
                }
                $table[] = $data;
                $i++;
            }
        }

        return $table;
    }

    private function structure($table_name_db)
    {
        $result = DB::select('SELECT distinct(COLUMN_NAME)
                          FROM INFORMATION_SCHEMA . COLUMNS
                          WHERE table_name = "' . $table_name_db . '"');

        $result = $this->array_remove_keys($result, ['id', 'lead_id', 'created_time', 'formulario_id', 'formulario', 'origen', 'tipoFormulario', 'habeas', 'terminos', 'updated_at', 'landing_identificador', 'fechaCreacion', 'Nombredeformulario', 'canal_sistema', 'habeas_sistema', 'terminos_sistema', 'proyecto']);

        $aux = array();
        foreach ($result as $item) {
            $aux[] = $item->COLUMN_NAME;
        }

        return $aux;
    }

    private function leads($table_name_db, $structure)
    {
        $select = implode(',', $structure);

        $result = DB::select('SELECT ' . $select . '
                          FROM ' . $table_name_db . '
                          ORDER BY created_at DESC');

        $aux = array();
        foreach ($result as $item) {
            $aux[] = (array)$item;
        }

        return $aux;
    }

    private function array_remove_keys($array, $keys)
    {
        $j = 0;
        foreach ($array as $item) {
            for ($i = 0; $i <= count($keys) - 1; $i++) {
                if ($item->COLUMN_NAME == $keys[$i]) {
                    unset($array[$j]);
                }
            }
            $j++;
        }

        $array = array_values($array);

        return $array;
    }

    function formularios()
    {
        $this->conexionFacebook();

        $page = new Page(env('PAGE_ID'));
        $leadgen_forms = $page->getLeadgenForms();

        foreach ($leadgen_forms as $form) {
            $leads = $form->getLeads();
            $csv = $form->getData()['leadgen_export_csv_url'];

            $a = 9;
        }
    }

    function crearEstructura(Request $request)
    {
        $this->conexionFacebook();
        $formulario = Formulario::where('form_id', $request['form_id'])->first();
        $form = new LeadgenForm($request['form_id']);
        $fields = $form->getFields();
        $leads = $form->getLeads();
        foreach ($leads as $lead) {
            $data = $lead->getData();
            $fields = $data['field_data'];
            break;
        }

        try {
            \Illuminate\Support\Facades\Schema::create('form-' . $request['form_id'], function (Blueprint $table) use ($fields) {
                $table->increments('id');
                foreach ($fields as $field) {
                    $table->string(FuncionesProvider::$field['name']);
                }
                $table->timestamps();
            });
        } catch (\Illuminate\Database\QueryException $e) {
            echo implode(',', $e->errorInfo);
        }
        $formulario->db_name = 'form-' . $request['form_id'];
        $formulario->con_estructura = true;
        $formulario->save();

    }

    function asociarOrigen()
    {
        $asociaciones = AsociacionCamposServicios::where('campo_servicio_crm_id', '=', 14)->get();

        foreach ($asociaciones as $asc) {
            $asc->campo_formulario = 'origen';
            $asc->save();
        }

        $asociaciones = LandingsCamposServicio::where('campos_servicios_crm_id', '=', 14)->get();

        foreach ($asociaciones as $asc) {
            $asc->campo_formulario = 'origen';
            $asc->save();
        }

        print_r('Finalizo asociación.');
    }

    function crearCampo()
    {
        $landings = Landing::all();
        foreach ($landings as $landing) {
            \Illuminate\Support\Facades\DB::transaction(function () use ($landing) {
                \Illuminate\Support\Facades\Schema::table($landing->db_name, function (\Illuminate\Database\Schema\Blueprint $table) {
                    $table->string('origen', 12)->default('web');
                });
            });
        }

        print_r('Finalizo crear campo.');
    }

    public function export_excel_leads_forms()
    {
        $leads['leads'] = DB::table('formularios_enviados_x_servicios as fe')->select('fe.id', 'fe.registro_id', 'fe.datos_envio', 'fe.respuesta', 'fe.formulario_id', 'f.nombre', 'f.db_name', 'ee.nombre as estado')
            ->join('formularios as f', 'f.id', '=', 'fe.formulario_id')
            ->join('estados_envios as ee', 'ee.id', '=', 'fe.estado_id')
            ->where('fe.servicio_crm_id', '=', 1)
            ->whereNotNull('fe.datos_envio')
            ->groupBy('fe.id')
            ->get();

        $leads['enviados'] = DB::table('formularios_enviados_x_servicios as fe')->select('fe.id')
            ->join('formularios as f', 'f.id', '=', 'fe.formulario_id')
            ->join('estados_envios as ee', 'ee.id', '=', 'fe.estado_id')
            ->where('fe.servicio_crm_id', '=', 1)
            ->where('fe.estado_id', '=', 1)
            ->whereNotNull('fe.datos_envio')
            ->groupBy('fe.id')
            ->get();

        $leads['rechazados'] = DB::table('formularios_enviados_x_servicios as fe')->select('fe.id')
            ->join('formularios as f', 'f.id', '=', 'fe.formulario_id')
            ->join('estados_envios as ee', 'ee.id', '=', 'fe.estado_id')
            ->where('fe.servicio_crm_id', '=', 1)
            ->where('fe.estado_id', '=', 2)
            ->whereNotNull('fe.datos_envio')
            ->groupBy('fe.id')
            ->get();

        $data = array();
        foreach ($leads['leads'] as $item) {
            $lead['datos_envio'] = $item->datos_envio;
            $lead['respuesta'] = $item->respuesta;
            $lead['formulario_id'] = $item->formulario_id;
            $lead['formulario'] = $item->nombre;
            $lead['estado'] = $item->estado;
            $data[] = $lead;
        }

        Excel::create('Leads Forms ' . date('d-m-Y'), function ($excel) use ($data) {
            $excel->sheet('Leads', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export("xls");

        dd('Total de Leads: ' . count($leads['leads']) . "\n" . 'Total enviados: ' . count($leads['enviados']) . "\n" . 'Total rechazados: ' . count($leads['rechazados']) . "\n");
    }

    public function export_excel_leads_landings()
    {
        $leads['leads'] = DB::table('landings_enviados_x_servicios as le')->select('le.id', 'le.registro_id', 'le.datos_envio', 'le.respuesta', 'le.landing_id', 'l.nombre', 'l.db_name', 'ee.nombre as estado')
            ->join('landings as l', 'l.id', '=', 'le.landing_id')
            ->join('estados_envios as ee', 'ee.id', '=', 'le.estados_envio_id')
            ->where('le.servicios_crm_id', '=', 1)
            ->whereNotNull('le.datos_envio')
            ->groupBy('le.id')
            ->get();

        $leads['enviados'] = DB::table('landings_enviados_x_servicios as le')->select('le.id')
            ->join('landings as l', 'l.id', '=', 'le.landing_id')
            //->join('estados_envios as ee', 'ee.id', '=', 'le.estados_envio_id')
            ->where('le.servicios_crm_id', '=', 1)
            ->where('le.estados_envio_id', '=', 1)
            ->whereNotNull('le.datos_envio')
            ->groupBy('le.id')
            ->get();

        $leads['rechazados'] = DB::table('landings_enviados_x_servicios as le')->select('le.id')
            ->join('landings as l', 'l.id', '=', 'le.landing_id')
            //->join('estados_envios as ee', 'ee.id', '=', 'le.estados_envio_id')
            ->where('le.servicios_crm_id', '=', 1)
            ->where('le.estados_envio_id', '=', 2)
            ->whereNotNull('le.datos_envio')
            ->groupBy('le.id')
            ->get();

        $data = array();
        foreach ($leads['leads'] as $item) {
            $lead['datos_envio'] = $item->datos_envio;
            $lead['respuesta'] = $item->respuesta;
            $lead['landing_id'] = $item->landing_id;
            $lead['landing'] = $item->nombre;
            $lead['estado'] = $item->estado;
            $data[] = $lead;
        }

        Excel::create('Leads Landings ' . date('d-m-Y'), function ($excel) use ($data) {
            $excel->sheet('Leads', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export("xls");

        dd('Total de Leads: ' . count($leads['leads']) . "\n" . 'Total enviados: ' . count($leads['enviados']) . "\n" . 'Total rechazados: ' . count($leads['rechazados']) . "\n");
    }

}
