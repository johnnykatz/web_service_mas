<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Formulario;
use App\Models\Admin\Landing;
use App\Models\Admin\Lead;
use App\Providers\FuncionesProvider;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Common\BaseRepository;

class LeadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Lead::class;
    }

    public function getLeadsFilter($filter, $export = false)
    {
        $table = $this->get_leads();
        $result['select_origin'] = $this->getOrigins($table);
        $result['select_form'] = $this->getForms($table);

        if ($filter['origin'] != '') {
            $table = array_filter($table, function ($item) use ($filter) {
                return $item['origin'] == $filter['origin'];
            });
            $table = array_values($table);
        }

        if ($filter['form'] != '') {
            $table = array_filter($table, function ($item) use ($filter) {
                return $item['form'] == $filter['form'];
            });
            $table = array_values($table);
        }

        if ($filter['form'] != '') {
            $table = array_filter($table, function ($item) use ($filter) {
                return $item['form'] == $filter['form'];
            });
            $table = array_values($table);
        }

        if ($filter['created_at'] != '') {
            $date = explode('-', $filter['created_at']);
            $date[0] = FuncionesProvider::date_bd(trim($date[0]), 'date');
            $date[1] = FuncionesProvider::date_bd(trim($date[1]), 'date');
            $table = array_filter($table, function ($item) use ($date) {
                return strtotime($item['created_at']) > strtotime($date[0] . ' 00:00:01') && strtotime($item['created_at']) < strtotime($date[1] . ' 23:59:59');
            });
            $table = array_values($table);
        }

        usort($table, function ($a, $b) {
            return strtotime($b['created_at']) - strtotime($a['created_at']);
        });

        $result['table'] = $table;

        return $result;
    }

    private function get_leads()
    {
        $landings = Formulario::where('con_estructura', true)->get();
        $table = array();

        foreach ($landings as $l) {
            $structure = $this->structure($l->db_name);
            $leads = $this->leads($l->db_name, $structure);
            $i = 0;
            foreach ($leads as $lead) {
                $data = array('fullname' => null, 'email' => null, 'phone' => null, 'city' => null, 'created_at' => null, 'form' => null);
                foreach ($lead as $key => $value) {
                    if ($key == 'fullname' || $key == 'nombre' || $key == 'name' || $key == 'firstname' || $key == 'lastname' || $key == 'full_name' || $key == 'nombres') {
                        $data['fullname'] .= trim($leads[$i][$key]) . ' ';
                    }
                    if ($key == 'email') {
                        $data['email'] = trim($leads[$i][$key]);
                    }
                    if ($key == 'phonenumber' || $key == 'celular' || $key == 'phone' || $key == 'telefono') {
                        $data['phone'] = trim($leads[$i][$key]);
                    }
                    if ($key == 'city' || $key == 'ciudad') {
                        $data['city'] = trim($leads[$i][$key]);
                    }
                    if ($key == 'formulario' || $key == 'Nombredeformulario') {
                        $data['form'] = trim($leads[$i][$key]);
                    }
                    $data['origin'] = $leads[$i]['origen'];
                    $data['created_at'] = $leads[$i]['created_at'];
                }
                $table[] = $data;
                $i++;
            }
        }

        $landings = Landing::where('con_estructura', true)->get();

        foreach ($landings as $l) {
            $structure = $this->structure($l->db_name);
            $leads = $this->leads($l->db_name, $structure);
            $i = 0;
            foreach ($leads as $lead) {
                $data = array('fullname' => null, 'email' => null, 'phone' => null, 'city' => null, 'created_at' => null, 'form' => null);
                foreach ($lead as $key => $value) {
                    if ($key == 'fullname' || $key == 'nombre' || $key == 'name' || $key == 'firstname' || $key == 'lastname' || $key == 'full_name' || $key == 'nombres') {
                        $data['fullname'] .= trim($leads[$i][$key]) . ' ';
                    }
                    if ($key == 'email') {
                        $data['email'] = trim($leads[$i][$key]);
                    }
                    if ($key == 'phonenumber' || $key == 'celular' || $key == 'phone' || $key == 'telefono') {
                        $data['phone'] = trim($leads[$i][$key]);
                    }
                    if ($key == 'city' || $key == 'ciudad') {
                        $data['city'] = trim($leads[$i][$key]);
                    }
                    if ($key == 'formulario' || $key == 'Nombredeformulario') {
                        $data['form'] = trim($leads[$i][$key]);
                    }
                    $data['origin'] = $leads[$i]['origen'];
                    $data['created_at'] = $leads[$i]['created_at'];
                }
                $table[] = $data;
                $i++;
            }
        }

        return $table;
    }

    private function structure($table_name_db)
    {
        $result = DB::select('SELECT distinct(COLUMN_NAME)
                          FROM INFORMATION_SCHEMA . COLUMNS
                          WHERE table_name = "' . $table_name_db . '"');

        $result = $this->array_remove_keys($result, ['id', 'lead_id', 'created_time', 'formulario_id', 'tipoFormulario', 'habeas', 'terminos', 'updated_at', 'landing_identificador', 'fechaCreacion', 'canal_sistema', 'habeas_sistema', 'terminos_sistema', 'proyecto']);

        $aux = array();
        foreach ($result as $item) {
            $aux[] = $item->COLUMN_NAME;
        }

        return $aux;
    }

    private function leads($table_name_db, $structure)
    {
        $select = implode(',', $structure);

        $result = DB::select('SELECT ' . $select . '
                          FROM ' . $table_name_db . '
                          ORDER BY created_at DESC');

        $aux = array();
        foreach ($result as $item) {
            $aux[] = (array)$item;
        }

        return $aux;
    }

    private function array_remove_keys($array, $keys)
    {
        $j = 0;
        foreach ($array as $item) {
            for ($i = 0; $i <= count($keys) - 1; $i++) {
                if ($item->COLUMN_NAME == $keys[$i]) {
                    unset($array[$j]);
                }
            }
            $j++;
        }

        $array = array_values($array);

        return $array;
    }

    private function getOrigins($array)
    {
        $aux = array();
        for ($i = 0; $i <= count($array) - 1; $i++) {
            $aux[] = $array[$i]['origin'];
        }

        $aux = array_unique($aux);

        natsort($aux);
        $aux = array_values($aux);

        $array = null;
        for ($i = 0; $i <= count($aux) - 1; $i++) {
            if ($aux[$i] != '') {
                $array[$aux[$i]] = $aux[$i];
            }
        }

        return $array;
    }

    private function getForms($array)
    {
        $aux = array();
        for ($i = 0; $i <= count($array) - 1; $i++) {
            $aux[] = $array[$i]['form'];
        }

        $aux = array_unique($aux);

        natsort($aux);
        $aux = array_values($aux);

        $array = null;
        for ($i = 0; $i <= count($aux) - 1; $i++) {
            if ($aux[$i] != '') {
                $array[$aux[$i]] = $aux[$i];
            }
        }

        return $array;
    }
}
