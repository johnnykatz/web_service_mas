<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Crm
 * @package App\Models\Admin
 */
class Lead extends Model
{
    public $table = '';

    public $fillable = [
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
