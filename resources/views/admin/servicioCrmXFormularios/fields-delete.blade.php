<h2>Elimiar configuracion</h2>
{!! Form::open(['route' => 'admin.servicioCrmXFormularios.store']) !!}
<div class="form-group col-sm-12">
    {!! Form::label('eliminar', 'Eliminar sincronizacion:') !!}
    {!! Form::checkbox('eliminar')!!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-danger']) !!}
</div>
{!! Form::close() !!}