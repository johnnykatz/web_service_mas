<div class="table-responsive">
    <table class="table table-responsive" id="landings-table">
        <thead>
        <tr>
            <td colspan="10" class="text-right">
                Nº Registros: {!! number_format(count($leads), 0, '.', '') !!}
            </td>
        </tr>
        </thead>
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Teléfono</th>
            <th colspan="1">Ciudad</th>
            <th>Formulario</th>
            <th>Origen</th>
            <th>Creado</th>
        </tr>
        </thead>
        <tbody>
        @foreach($leads as $item)
            <tr>
                <td>{!! strtoupper($item['fullname']) !!}</td>
                <td>{!! strtolower($item['email']) !!}</td>
                <td>{!! trim($item['phone']) !!}</td>
                @if(is_null($item['city']))
                    <td colspan="1">--</td>
                @else
                    <td colspan="1">{!! $item['city'] !!}</td>
                @endif
                <td>{!! $item['form'] !!}</td>
                <td>{!! $item['origin'] !!}</td>
                <td>{!! date('d/m/Y H:i:s', strtotime($item['created_at'])) !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>