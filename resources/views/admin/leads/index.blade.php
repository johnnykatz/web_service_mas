@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Leads</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-body">
                {!! Form::open(['route' => 'admin.leads.index', 'method' => 'GET', 'id' => 'form_leads']) !!}

                @include('admin.leads.filters')

                {!! Form::close() !!}
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-body">

                @include('admin.leads.table')

            </div>
        </div>
    </div>
@endsection

