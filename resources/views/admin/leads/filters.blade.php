@section("scripts")
    <script>
        $(document).ready(function () {
            inicializarDateRange();

            $('#form').select2();
            $('#origin').select2();

            $(document).ready(function () {
                $('#btn-exportar-excel').click(function () {
                    $('#form_leads').attr('target', '_blank');
                    $('#form_leads').attr('action', '{!! route('admin.leads.export_leads') !!}');
                    $('#form_leads').submit();
                    $('#form_leads').removeAttr('target');
                    $('#form_leads').removeAttr('action');
                });
            });
        })
    </script>
@endsection

<!-- Created_at Field -->
<div class="form-group col-sm-4">
    {!! Form::label('created_at', 'Fecha Creación:') !!}
    {!! Form::text('created_at',(isset($filtro['created_at']))? $filtro['created_at']:'',['class'=>'form-control daterange', 'readonly']) !!}
</div>

<!-- Form Field -->
<div class="form-group col-sm-4">
    {!! Form::label('form', 'Formulario:') !!}
    {!! Form::select('form',$forms,(isset($filters['form']))? $filters['form']:'',['class'=>'form-control','placeholder'=>'Seleccione...', 'id' => 'form']) !!}
</div>

<!-- Origin Field -->
<div class="form-group col-sm-4">
    {!! Form::label('origin', 'Origin:') !!}
    {!! Form::select('origin',$origins,(isset($filters['origin']))? $filters['origin']:'',['class'=>'form-control','placeholder'=>'Seleccione...', 'id' => 'origin']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-right">
    <button class="btn btn-success btn-label-left" type="button" id="btn-exportar-excel" title="Exportar Excel">
        <span><i class="fa fa-file-excel-o"></i></span>
    </button>
    {!! Form::submit('Buscar', ['class' => 'btn btn-info']) !!}
</div>
