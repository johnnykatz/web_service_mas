<li class="{{ Request::is('tokens*') ? 'active' : '' }}">
    <a href="{!! route('admin.tokens.index') !!}">
        <i class="fa fa-edit"></i>
        <span>Tokens</span>
    </a>
</li>

<li class="{{ Request::is('formularios*') ? 'active' : '' }}">
    <a href="{!! route('admin.formularios.index') !!}">
        <i class="fa fa-edit"></i>
        <span>Formularios</span>
    </a>
</li>

<li class="{{ Request::is('landings*') ? 'active' : '' }}">
    <a href="{!! route('admin.landings.index') !!}">
        <i class="fa fa-edit"></i>
        <span>Landings</span>
    </a>
</li>

<li class="{{ Request::is('crms*') ? 'active' : '' }}">
    <a href="{!! route('admin.crms.index') !!}">
        <i class="fa fa-edit"></i>
        <span>Crms</span>
    </a>
</li>

<li class="{{ Request::is('servicioCrms*') ? 'active' : '' }}">
    <a href="{!! route('admin.servicioCrms.index') !!}">
        <i class="fa fa-edit"></i>
        <span>ServicioCrms</span>
    </a>
</li>

<li class="{{ Request::is('campoServicioCrms*') ? 'active' : '' }}">
    <a href="{!! route('admin.campoServicioCrms.index') !!}">
        <i class="fa fa-edit"></i>
        <span>CampoServicioCrms</span>
    </a>
</li>

<li class="{{ Request::is('leads*') ? 'active' : '' }}">
    <a href="{!! route('admin.leads.index') !!}">
        <i class="fa fa-edit"></i>
        <span>Leads</span>
    </a>
</li>